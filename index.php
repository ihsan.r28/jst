<link rel="stylesheet" type="text/css" 	href="inc/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" 	href="inc/css/bootstrap-theme.min.css">
<script type="text/javascript" src="inc/js/jquery.js"></script>
<script type="text/javascript" src="inc/js/bootstrap.min.js"></script>


<div class="col-md-4" style="margin-top: 40px;">
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Input Hebb Rule</h3>
	  </div>
	  <div class="panel-body">
			<form action="hebb.php" method="post">
				<div class="form-group">
				  <label for="">Input X1 :</label>
				  <input type="text" name="x1" class="form-control" id="" placeholder="Pisahkan nilai dengan koma, contoh : 2,3,5,8" required>
				</div>
				<div class="form-group">
				  <label for="">Input X2 :</label>
				  <input type="text" name="x2" class="form-control" id="" placeholder="Pisahkan nilai dengan koma, contoh : 2,3,5,8" required>
				</div>
				<div class="form-group">
				  <label for="">Input T :</label>
				  <input type="text" name="t" class="form-control" id="" placeholder="Pisahkan nilai dengan koma, contoh : 2,3,5,8" required>
				</div>
				<div class="form-group">
				  <button type="submit" class="btn btn-default">
						Submit
				  </button>
				</div>
			</form>
	  </div>
	</div>
</div>

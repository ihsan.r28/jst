<link rel="stylesheet" type="text/css" 	href="inc/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" 	href="inc/css/bootstrap-theme.min.css">
<script type="text/javascript" src="inc/js/jquery.js"></script>
<script type="text/javascript" src="inc/js/bootstrap.min.js"></script>
<?php
	$x1=explode(",",$_POST['x1']);
	$x2=explode(",",$_POST['x2']);
	$t=explode(",",$_POST['t']);

	// $x1=array("1","4","2","1");
	// $x2=array("1","2","1","4");
	// $t=array("0","1","0","1");
	$loop=count($x1);

	$dw1;
	for ($i=0; $i < $loop; $i++) {
		$totd=$x1[$i]*$t[$i];
		$dw1[$i]=$totd;
	}

	$dw2;
	for ($j=0; $j < $loop; $j++) {
		$tot=$x2[$j]*$t[$j];
		$dw2[$j]=$tot;
	}

	$w1;
	$totw1=0;
	for ($k=0; $k < $loop; $k++) {
		$totw1=$totw1+$dw1[$k];
		$w1[$k]=$totw1;
	}

	$w2;
	$totw2=0;
	for ($l=0; $l < $loop; $l++) {
		$totw2=$totw2+$dw2[$l];
		$w2[$l]=$totw2;
	}

	$net;
	for ($z=0; $z< $loop; $z++) {
		$totnet=($x1[$z]*$w1[3])+($x2[$z]*$w2[3]);
		$net[$z]=$totnet;
	}

	$ynet;
	for ($e=0; $e< $loop; $e++) {
		if ($net<0) {
			$ynet[$e]=0;
		}
		else {
			$ynet[$e]=0;
		}
	}

?>

<div class="col-md-12" style="margin-top: 40px">
	<a class="btn btn-primary" href="index.php"><< Kembali</a>
</div>

<div class="col-md-6" style="margin-top: 20px">
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Hebb Rulea/h3></div>

  <!-- Table -->
	  <table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th rowspan="2">X1</th>
					<th rowspan="2">X2</th>
					<th rowspan="2">T</th>
					<th colspan="2">Perubahan Bobot</th>
					<th colspan="2">Bobot Akhir</th>
				</tr>
				<tr>
					<th>∆W1</th>
					<th>∆W2</th>
					<th>W1=0</th>
					<th>W2=0</th>
				</tr>
			</thead>
			<tbody>
				<?php
					for ($i=0; $i < $loop; $i++) { ?>
						<tr>
							<td><?php echo $x1[$i] ?></td>
							<td><?php echo $x2[$i] ?></td>
							<td><?php echo $t[$i] ?></td>
							<td><?php echo $dw1[$i] ?></td>
							<td><?php echo $dw2[$i] ?></td>
							<td><?php echo $w1[$i] ?></td>
							<td><?php echo $w2[$i] ?></td>
						</tr>
				<?php
					}
				?>
			</tbody>
	  </table>
	</div>
</div>

<div class="col-md-6" style="margin-top: 20px">
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Testing</h3></div>

  <!-- Table -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th >X1</th>
				<th >X2</th>
				<th >Net W1.X1 + W2.X2</th>
				<th >Y(net)</th>
				<th >Target</th>
				<th >Keterangan</th>
			</tr>
		</thead>
		<tbody>
			<?php
				for ($i=0; $i < $loop; $i++) { ?>
					<tr>
						<td><?php echo $x1[$i] ?></td>
						<td><?php echo $x2[$i] ?></td>
						<td><?php echo $net[$i] ?></td>
						<td><?php echo $ynet[$i] ?></td>
						<td><?php echo $t[$i] ?></td>
						<?php if ($ynet[$i]!=$t[$i]) {
							echo "<td>Sesuai</td>";
						} else {
							echo "<td>Tidak Sesuai</td>";
						}?>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
	</div>
</div>
